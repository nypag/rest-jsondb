# README

Zweck dieses Projektes ist es ein REST-Api zu verfügung zu stellen. Für dies wird das bestehende package json-server mit json-server-auth und express genutzt.

### Beschreibung

Die Basis dieses Servers ist das package json-server-auth:
[link zu der Dokumentation](https://github.com/jeremyben/json-server-auth#readme)
Die Dokumentation über die Authentifizierung und Authorisierung ist dort abgelegt. Basis ist jedenfalls die Datei routes

Beim builden des Images werden alle json-Dateien aus dem Verzeichnis **jsons** miteinander zu der Datei db zusammengefügt. Der json-server startet dann. Zusätzlich können jedoch die Routes abgesichert werden. Dies geschieht mittels dem File routes.json. Aktuell sind **alle** Endpoints abgesichert.

Folgende Dateien (somit Endpoints) gibt es in diesem Image:
(Bitte immer hier nachführen, wenn Datein entfernt oder hinzugefügt werden)

- address
- address_status
- author
- book
- book_author
- book_language
- cars
- country
- cust_order
- customer
- customer_address
- employee
- job
- order_history
- order_line
- order_status
- penguins
- pokemons
- publisher
- rickandmorty
- shipping_method
- smartphone
- games
- bands
- mangas
- spanische-staedte

### Wie erstelle ich das Image?

`docker build  --platform linux/amd64 -t devnyzh/rest-jsondb .`

### Image in unsere private Registry pushen (nur wenn Änderungen vorgenommen wurden)
Zuerst einloggen. Credentials bei Luca/Marco/Gianluca nachfragen.
`docker login -u devnyzh -p 'beimAusbildnerFragen' https://registry.noseryoung.ch`
danach pushen
`docker push registry.noseryoung.ch/noseryoung/restdb`

### Image in die public Registry pushen, damit Lernende zugriff haben.

`docker push devnyzh/rest-jsondb`

### Wie starte ich einen container?
Vorgängig muss man sich in unsere Registry einloggen. Password 

`docker run -p 3000:3000 --name restdb -d devnyzh/rest-jsondb`

da jedoch der Port 3000 meistens von React benutzt wird, lieber einen anderen Port nutzen z.Bsp 3030:

`docker run -p 3030:3000 --name restdb -d devnyzh/rest-jsondb`
