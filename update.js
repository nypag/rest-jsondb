"use strict";

const fs = require("fs");

function randomDate() {
  var startDate = new Date(1940, 0, 1).getTime();
  var endDate = new Date(2010, 0, 1).getTime();
  var spaces = endDate - startDate;
  var timestamp = Math.round(Math.random() * spaces);
  timestamp += startDate;
  return new Date(timestamp);
}

let rawdata = fs.readFileSync("jsons/persons.json");
let personen = JSON.parse(rawdata);
console.log(personen);
personen.persons.forEach(async (person) => {
  const newBirthday = randomDate();
  person.birthdate = newBirthday.toLocaleDateString();
});

fs.writeFileSync("gaga.json", JSON.stringify(personen));

console.log(personen);
