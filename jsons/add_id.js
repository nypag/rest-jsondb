const fs = require("fs");

// Lese die JSON-Datei
const data = fs.readFileSync("penguins.json");

// Konvertiere den Inhalt in ein JavaScript-Objekt
const obj = JSON.parse(data);

// Füge jedem Objekt im cars-Array ein neues Schlüssel-Wert-Paar hinzu
for (let i = 0; i < obj.penguins.length; i++) {
  obj.penguins[i].id = i + 1;
}

// Schreibe das aktualisierte Objekt in die Datei
fs.writeFileSync("datei.json", JSON.stringify(obj));
