const fs = require("fs");

// Lese die JSON-Datei
const data = fs.readFileSync("cars.json");

// Konvertiere den Inhalt in ein JavaScript-Objekt
const obj = JSON.parse(data);

// Füge jedem Objekt im cars-Array ein neues Schlüssel-Wert-Paar hinzu
for (let i = 0; i < obj.cars.length; i++) {
  obj.cars[i].imgUrl = "";
}

// Schreibe das aktualisierte Objekt in die Datei
fs.writeFileSync("datei.json", JSON.stringify(obj));
